# motor-titan

Implementation of [my Lua ECS library](https://github.com/Andre-LA/Motor) in [Titan](http://titan-lang.org)

---
In english (translated in G. Translator :|)

This is an experiment.

I rewrote my ECS library originally made in Lua for Titan.

However, I had to adapt my library since Titan is still in development and does not have all the necessary features to create a perfect "translation".

Curiously, this adaptation left (in my view) the library more organized than before, and I even want to rewrite my original library to be compatible with this new version, so it can be said that Titan taught me how to better organize my source codes in Lua.

In the end, it was a very fun and curious experience for me and I intend to play more with the Titan language.

I also want to try the [Terra language](http://terralang.org/) at some point. :)

---

In portuguese (original text)

Este é um experimento.

Eu reescrevi minha biblioteca ECS feita originalmente em Lua para Titan.

Entretanto, eu tive que adaptar minha biblioteca visto que Titan ainda está em desenvolvimento e não tem todas as características necessárias para criar uma "tradução" perfeita.

Curiosamente, essa adaptação deixou (ao meu ver) o código mais organizado que antes e pretendo até mesmo reescrever minha biblioteca original para que seja compatível com essa nova versão, então, pode-se dizer que Titan me ensinou como organizar melhor meus códigos fontes em Lua.

No final, foi uma experiência muito divertida e curiosa pra mim e pretendo brincar mais com a linguagem Titan. 

Também pretendo experimentar a [linguagem Terra](http://terralang.org/) em algum momento. :)