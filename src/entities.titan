local bit_ids  = import "bit_ids"
local storages = import "storages"

record Entity
  state_data_bit_filter: {integer}
  associated_components_entries_ids: {storages.Id}
  associated_state_data_bit_ids: {{integer}}
end

function new_entity(): Entity
  local entity: Entity = {
    state_data_bit_filter = {0, 0, 0, 0},
    associated_components_entries_ids = {},
    associated_state_data_bit_ids = {}
  }

  return entity
end

function Entity:find_associated_id(id: storages.Id): integer
  for i = 1, #self.associated_components_entries_ids do
    local entity_entry_id: storages.Id = self.associated_components_entries_ids[i]

    if entity_entry_id.index == id.index and entity_entry_id.generation == id.generation  then
      return i
    end
  end

  return 0
end

function Entity:find_associated_bit_id(bit_id: {integer}): integer
  for i = 1, #self.associated_state_data_bit_ids do
    if bit_ids.equals(self.associated_state_data_bit_ids[i], bit_id) then
      return i
    end
  end

  return 0
end

function Entity:associate_component(component_entry_id: storages.Id, component_state_data_bit_id: {integer})
  bit_ids.add_in_bit_filter(self.state_data_bit_filter, component_state_data_bit_id)
  self.associated_components_entries_ids[#self.associated_components_entries_ids+1] = component_entry_id
  self.associated_state_data_bit_ids[#self.associated_state_data_bit_ids+1] =  component_state_data_bit_id
end

function Entity:disassociate_component(component_entry_id: storages.Id, component_state_data_bit_id: {integer})
  bit_ids.remove_in_bit_filter(self.state_data_bit_filter, component_state_data_bit_id)
  self.associated_components_entries_ids[self:find_associated_id(component_entry_id)] = nil
  self.associated_state_data_bit_ids[self:find_associated_bit_id(component_state_data_bit_id)] = nil
end
